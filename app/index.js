import Service from "../src";

const verbose = false;

// Inserts an iframe into a given container
function insertFrame({ container, src, backgroundColor = "white" }) {

  container.iframes = container.iframes || {};

  if (container.iframes[src]) {
    container.iframes[src].remove();
  }

  const frame = document.createElement('iframe');
  frame.setAttribute('src', src);
  frame.style.width = "100%";
  frame.style.height = "200px";
  frame.style.backgroundColor = backgroundColor;
  container.appendChild(frame);
  container.iframes[src] = frame;
}

// Initiates the router behavior
async function runAsRouter({ container }) {

  // Create a service instance having router behavior
  const service = await Service({ id: "focus", verbose, router: true });

  service.on('connected', (client) => {
    // console.log(client);
  });

  // const router = await service.findRouter();
  // if (router) {
  //   await service.connect(router);
  // }

  service.register({
    method: "whoami",
    handler: () => 'I am Server'
  });

  service.register({
    method: "random",
    handler: () => Math.random()
  });

  service.registerProxy({ alias: "whoIsLocus", method: "whoami", service: "locus" });

  service.registerProxy({ alias: "whoIsAnalytics", method: "whoami", service: "analytics" });

  service.registerProxy({ alias: "whoIsSibling", method: "whoami", service: "sibling" });

  // SIBLING SERVICE
  const sibling = await Service({ id: "sibling", verbose });

  setTimeout(async () => {
    // const siblingRouter = await sibling.findRouter({ where: { client: ["locus"] } });
    // const siblingRouter = await sibling.findRouter({ where: { client: ["locus"] } });
    const siblingRouter = await sibling.findRouter();
    if (siblingRouter) {
      await sibling.connect(siblingRouter);
    }
  }, 3000);

  sibling.register({
    method: "whoami",
    handler: () => 'I am Sibling'
  });

  // Create UI elements for the server frame
  const headerContainer = document.createElement('div');
  const header = document.createElement('h1');
  header.innerText = "Focus Frame (Service & Router)";
  headerContainer.appendChild(header);
  container.appendChild(headerContainer);

  let result;

  // Create a button for publishing to locus
  const sendToLocusButton = document.createElement('button');
  sendToLocusButton.innerText = "Send to Locus";
  sendToLocusButton.addEventListener('click', async () => {
    result = await service.whoIsLocus(null, {
      callback: (event) => {
        console.log('Callback', event);
      }
    });
    // result = await service.whoIsLocus(null);
    result && console.log(result);
  });
  headerContainer.appendChild(sendToLocusButton);

  // Create a button for publishing to analytics
  const sendToAnalyticsButton = document.createElement('button');
  sendToAnalyticsButton.innerText = "Send to Analytics";
  sendToAnalyticsButton.addEventListener('click', async () => {
    result = await service.whoIsAnalytics();
    result && console.log(result);
  });
  headerContainer.appendChild(sendToAnalyticsButton);

  // Create a button for sibling
  const sendToSiblingButton = document.createElement('button');
  sendToSiblingButton.innerText = "Send to Sibling";
  sendToSiblingButton.addEventListener('click', async () => {
    result = await service.whoIsSibling();
    result && console.log(result);
  });
  headerContainer.appendChild(sendToSiblingButton);


  insertFrame({ container, src: "./client.html", backgroundColor: "lightblue" });
  insertFrame({ container, src: "./analytics.html", backgroundColor: "lightgreen" });

  // insertFrame({ container, src: "http://localhost:58795/client.html", backgroundColor: "lightblue" });
  // insertFrame({ container, src: "http://localhost:58795/analytics", backgroundColor: "lightgreen" });

  // // // Insert client frames
  // setInterval(() => {    
  //   insertFrame({ container, src: "http://localhost:58795/client.html", backgroundColor: "lightblue" });    
  //   insertFrame({ container, src: "http://localhost:58795/analytics", backgroundColor: "lightgreen" });
  // }, 5000);

}

// Initiates the Locus client behavior
async function runAsClientLocus({ container }) {
  const service = await Service({ id: "locus", verbose });

  let result;

  service.register({
    method: "whoami",
    handler: (args, context) => {
      if (context.hasCallback) {
        console.log('Callback', context);
        // throw new Error('Something went wrong!');
        service.proxy({ method: context.transactionId, args: "initializing", serviceId: context.serviceId });
      }
      return 'I am Locus!!!'
    }
  });

  service.registerProxy({ alias: "whoIsServer", method: "whoami" });
  service.registerProxy({ alias: "whoIsAnalytics", method: "whoami", service: "analytics" });
  service.registerProxy({ alias: "whoIsSibling", method: "whoami", service: "sibling" });

  const router = await service.findRouter({ where: { id: ["focus", "focusx"] } });
  if (router) {
    await service.connect(router);
  }

  // // UI interactions for Locus client
  // document.getElementById('disconnect').removeAttribute('disabled');
  // document.getElementById('disconnect').addEventListener('click', () => {
  //     client.disconnect();
  // });

  document.getElementById('sendToFocus').removeAttribute('disabled');
  document.getElementById('sendToFocus').addEventListener('click', async () => {
    result = await service.whoIsServer();
    result && console.log(result);
  });

  document.getElementById('sendToAnalytics').removeAttribute('disabled');
  document.getElementById('sendToAnalytics').addEventListener('click', async () => {
    result = await service.whoIsAnalytics();
    result && console.log(result);
  });

  document.getElementById('sendToFocusSibling').removeAttribute('disabled');
  document.getElementById('sendToFocusSibling').addEventListener('click', async () => {
    result = await service.whoIsSibling();
    result && console.log(result);
  });
}

// Initiates the Analytics client behavior
async function runAsClientAnalytics({ container }) {
  const service = await Service({ id: "analytics", verbose });

  let result;

  service.register({
    method: "whoami",
    handler: () => {
      // throw new Error('Something went wrong!!!');
      return 'I am Analytics!!!'
    }
  });

  const router = await service.findRouter({ where: { id: "focus" } });

  if (router) {
    result = await service.connect(router);
    result && console.log(result);
  }

  service.registerProxy({ alias: "whoIsServer", method: "whoami" });
  service.registerProxy({ alias: "whoIsLocus", method: "whoami", service: "locus" });
  service.registerProxy({ alias: "whoIsSibling", method: "whoami", service: "sibling" });

  // // UI interactions for Analytics client
  // document.getElementById('disconnect').removeAttribute('disabled');
  // document.getElementById('disconnect').addEventListener('click', () => {
  //     client.disconnect();
  // });

  document.getElementById('sendToFocus').removeAttribute('disabled');
  document.getElementById('sendToFocus').addEventListener('click', async () => {
    result = await service.whoIsServer();
    result && console.log(result);
  });

  document.getElementById('sendToLocus').removeAttribute('disabled');
  document.getElementById('sendToLocus').addEventListener('click', async () => {

    result = await service.whoIsLocus(null, {
      callback: (event) => {
        console.log('Callback', event);
      }
    });
    result && console.log(result);

    // setInterval(async ()=>{
    // },1);
  });

  document.getElementById('sendToFocusSibling').removeAttribute('disabled');
  document.getElementById('sendToFocusSibling').addEventListener('click', async () => {
    result = await service.whoIsSibling();
    result && console.log(result);
  });
}

// Main function to initiate based on the given ID
export default ({ container }) => {
  return async ({ id } = {}) => {
    if (id === 'locus') runAsClientLocus({ container });
    else if (id === 'analytics') runAsClientAnalytics({ container });
    else await runAsRouter({ container });
  }
}