# Developer Guide for @fnet/inter-service-js

## Overview

The `@fnet/inter-service-js` library provides a communication framework for connecting services running in different window contexts or frames. It facilitates message-passing between these services using the `postMessage` API, making it easier to build applications with multiple services communicating in a loosely-coupled manner. The library includes functionality for connecting services, proxying methods across services, and managing communication channels securely and efficiently.

## Installation

To install this library, use npm or yarn. Simply run:

```bash
npm install @fnet/inter-service-js
```

or

```bash
yarn add @fnet/inter-service-js
```

## Usage

To get started with `@fnet/inter-service-js`, you need to initialize the library and create communication channels between services.

```javascript
import interServiceJs from '@fnet/inter-service-js';

async function setupService() {
  const service = await interServiceJs({
    id: 'my-service-id',
    verbose: true,
    router: true,
  });

  // Example: Register a method that echoes the received message
  service.register({
    method: 'echo',
    handler: (args) => {
      console.log('Echo received:', args);
      return args;
    }
  });

  // Example: Connect to another service
  try {
    const result = await service.connect({
      window: otherWindow,
      serviceId: 'other-service-id'
    });
    console.log('Connected to service:', result);
  } catch (error) {
    console.error('Connection failed:', error.message);
  }
}

setupService();
```

## Examples

### Registering a Service Method

You can register methods that other services can call. For instance, to register a `greet` method:

```javascript
service.register({
  method: 'greet',
  handler: (args) => {
    return `Hello, ${args.name}`;
  }
});
```

### Proxying a Method Call to Another Service

If you need to call a method on another service:

```javascript
service.proxy({
  method: 'getTime',
  service: 'time-service-id',
  args: { timezone: 'UTC' }
}).then(response => {
  console.log('Current time in UTC:', response);
}).catch(error => {
  console.error('Failed to get time:', error.message);
});
```

### Finding and Connecting to a Router

Before connecting to a service, a router needs to be identified:

```javascript
service.findRouter({
  timeout: 2000,
  where: { serviceId: 'target-service-id' }
}).then(router => {
  if (router) {
    console.log('Router found:', router);
  } else {
    console.log('No router found within the timeout period.');
  }
});
```

### Unregistering a Service Method

To unregister a previously registered method:

```javascript
service.unregister({ method: 'greet' });
```

## Acknowledgement

The development of `@fnet/inter-service-js` leverages the `nanoid` library for generating unique identifiers and the `eventemitter3` for event handling. Special thanks to the developers and contributors of these libraries for providing essential functionality that enhances our library's capabilities.