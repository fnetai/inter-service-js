import handleConnect from './handle-connect';
import handleAreYouRouter from './handle-are-you-router';
import handleForwardToService from './handle-forward-to-service';
import handleServiceHandler from './handle-service-handler';
import handleTransactionHandler from './handle-transaction-handler';

export default async function (event) {

  if (typeof event.data !== 'object') return;

  const { transactionId, contextId, scriptId, messageId, serviceId, method, service } = event.data;

  if (!transactionId || !contextId || !scriptId || !messageId || !serviceId) {
    return;
  };

  this.verbose && console.log(`[IS] ${this.id}`, event.data);

  switch (method) {

    case 'connect':
      await handleConnect.call(this, event);
      break;

    case 'are-you-router':
      await handleAreYouRouter.call(this, event);
      break;

    default:
      // Forward call request to service
      if (method && service) await handleForwardToService.call(this, event);
      // Handle service registered handler
      else if (method && this.registeredHandlers[method]) await handleServiceHandler.call(this, event);
      // Handle transaction handler
      else if (this.transactionHandler[transactionId]) await handleTransactionHandler.call(this, event);
      break;
  }
}