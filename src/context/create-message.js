import { nanoid } from 'nanoid';

export default function (args) {
    return {
        ts: Date.now(),
        contextId: this.contextId,
        scriptId: this.scriptId,
        serviceId: this.serviceId,
        messageId: nanoid(),
        sender: this.id,
        ...args
    }
}