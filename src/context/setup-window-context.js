import { nanoid } from 'nanoid';


async function hashString(input) {
    if(!input) return null; 

    const msgBuffer = new TextEncoder().encode(input);                     // Convert the string to a buffer
    const hashBuffer = await crypto.subtle.digest('SHA-1', msgBuffer);   // Hash the buffer using SHA-1

    const hashArray = Array.from(new Uint8Array(hashBuffer));              // Convert buffer to byte array
    const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // Convert bytes to hex string
    return hashHex;
}

export default async function () {
    
    const key = 'INTERSERVICE_CONTEXT_ID';
    if (!window[key]) {
        Object.defineProperty(window, key, {
            value: nanoid(),
            enumerable: false,
            configurable: false
        });
    }

    this.contextId = window[key];

    this.scriptId = await hashString(import.meta.url);
}