export default function ({ transactionId, reject, timeout = 1000 }) {
  const timeoutId = setTimeout(() => {

    clearTimeout(timeoutId);

    delete this.transactionHandler[transactionId];

    this.api.unregister({ method: transactionId });

    reject(new Error('[IS] timeout'));
  }, timeout);
}