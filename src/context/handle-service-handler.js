export default async function (event) {
  const { transactionId, messageId, method } = event.data;

  const handler = this.registeredHandlers[method];

  const message = this.createMessage({
    transactionId,
    replyTo: messageId,
  });

  try {
    const result = await handler(event.data.body, event.data);
    message.body = result;
    event.target.postMessage(message);
  } catch (error) {
    message.error = error?.message || error;
    event.target.postMessage(message);
  }
}