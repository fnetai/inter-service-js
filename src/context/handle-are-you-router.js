import serviceMatch from "../api/service-match";

export default async function (event) {
  const { transactionId, messageId, serviceId, body } = event.data;

  if (this.router_enabled !== true) return;

  if (this.serviceId === serviceId) return;

  if (!serviceMatch({ where: body?.where, service: this })) return;

  const message = this.createMessage({
    transactionId,
    replyTo: messageId,
  });
  
  message.body =body;

  event.source.postMessage(message, "*");
}