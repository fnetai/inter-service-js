import serviceMatch from "../api/service-match";

export default async function (event) {
  const { transactionId, messageId, serviceId, method, sender, body } = event.data;

  if (this.serviceId === serviceId) return;
  
  if (!serviceMatch({ where: body?.where, service: this })) return;

  // TODO: Decide if we want to allow multiple connections from the same service
  // if (this.clients[sender])
  //  throw new Error(`[IS] ${this.id}: Connection already exists with ${sender}`);

  const client = new MessageChannel();

  client.port1.start();
  this.clients[sender] = client.port1;
  this.clients[sender].onmessage = this.handleMessage.bind(this);
  this.clients[sender].serviceId = serviceId;
  this.clients[sender].onmessageerror = (error) => {
    console.error(`[IS] ${this.id}: Error received from ${sender}`, error?.message || error);
  };
  const message = this.createMessage({
    transactionId,
    replyTo: messageId,
  });

  message.body=body;

  event.source.postMessage(message, '*', [client.port2]);
  this.verbose && console.log(`[IS] ${this.id}: Connection received from ${sender}`);

  try {
    this.ee.emit('connected', {
      contextId: event.data.contextId,
      scriptId: event.data.scriptId,
      serviceId: serviceId,
      service: sender,
    });
  } catch (error) {
    this.verbose && console.log(`[IS] ${this.id}: Error while emitting connected event`, error.message);
  }
}