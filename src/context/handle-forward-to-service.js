export default async function (event) {
  const { transactionId, messageId, method, service } = event.data;

  // forward to the service
  const message = this.createMessage({
    transactionId,
    replyTo: messageId,
  });

  try {
    const result = await this.api.proxy({ method, args: event.data.body, service });
    message.body = result;
    event.target.postMessage(message);
  } catch (error) {
    message.error = error?.message || error;
    event.target.postMessage(message);
  }
}