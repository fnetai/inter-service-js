export default async function (event) {
  const { transactionId } = event.data;

  const handler = this.transactionHandler[transactionId];

  delete this.transactionHandler[transactionId];
  
  await handler(event);
}