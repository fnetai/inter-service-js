export default function ({ method }) {
  if (!this.api[method]) return;

  delete this.api[method];
  delete this.registeredHandlers[method];

  this.verbose && console.log(`[IS] ${this.id}: Unregistered handler for ${method}`);
}