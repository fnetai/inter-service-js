import { nanoid } from 'nanoid';
import contextGlobal from '../context/context-global';
import serviceMatch from "./service-match";


// @ai: This is critical for conneting to a service. It should be called before connecting to a service. The found router details are used to establish a communication channel with the target service.
/**
 * @param {Object} args - The arguments object.
 * @param {number} [args.timeout=1000] - The maximum time to wait for a router to be found, in milliseconds.
 * @param {number} [args.interval=100] - The interval time between each attempt to find a router, in milliseconds.
 * @returns {Promise<Object|null>} A promise that resolves with the found router's details or null if not found.
 */
export default async function ({ timeout = 1000, interval = 100, where = {} } = {}) {
  const now = Date.now();
  let tryCount = Math.floor(timeout / interval);

  const finder = findRouter.bind(this);

  while (tryCount > 0) {
    const router = await finder({ timeout: interval, where });
    if (router) {
      this.verbose && console.log(`[IS] ${this.id}: Router found in ${Date.now() - now}ms`);
      return router;
    }
    tryCount--;
  }

  this.verbose && console.log(`[IS] ${this.id}: Router not found in ${Date.now() - now}ms`);
  return null;
}

/**
 * Finds the router by sending messages to the target window.
 * @param {Object} args - The arguments object.
 * @param {Window} args.window - The target window to send messages to.
 * @param {number} args.timeout - The timeout for finding the router.
 * @returns {Promise<Object>} A promise that resolves with the router details.
 */
function find({ window: targetWindow, timeout, where } = {}) {
  return new Promise((resolve, reject) => {
    const transactionId = nanoid();

    const handler = (event) => {
      if (typeof event.data !== 'object') return;

      const { contextId, scriptId, serviceId, messageId, sender } = event.data;

      if (!contextId || !scriptId || !serviceId || !messageId || !sender) return;

      resolve(event.data);
    };

    this.transactionHandler[transactionId] = handler;

    const message = this.createMessage({
      transactionId,
      method: 'are-you-router'
    });

    message.body = { where }

    targetWindow.postMessage(message, '*');

    this.setupTransactionTimeout({ transactionId, reject, timeout });
  });
}

/**
 * Finds a sibling router in the context.
 * @returns {Object|null} The sibling router's details or null if not found.
 */
function findSibling({ where } = {}) {
  const contextServices = contextGlobal.services;
  const services = Object.keys(contextServices);
  const currentServiceId = this.serviceId;

  for (let i = 0; i < services.length; i++) {
    const service = contextServices[services[i]];
    if (service.serviceId === currentServiceId) continue;

    if (!serviceMatch({ where, service })) continue;

    if (service.router_enabled) return service;
  }

  return null;
}

/**
 * Attempts to find a router in the parent windows and sibling frames.
 * @param {Object} args - The arguments object.
 * @param {number} args.timeout - The timeout for each attempt to find a router.
 * @returns {Promise<Object|null>} A promise that resolves with the found router's details or null if not found.
 */
async function findRouter({ timeout, where } = {}) {
  // Try to find a sibling router
  const siblingRouter = findSibling.call(this, { where });

  if (siblingRouter) {
    return {
      window: window,
      serviceId: siblingRouter.serviceId,
      contextId: siblingRouter.contextId,
      scriptId: siblingRouter.scriptId
    };
  }

  // Try to find a router in the parent windows
  let current = window.parent;

  while (current !== window) {
    const found = await find.call(this, { window: current, timeout, where })
      .catch((error) => {
        this.verbose && console.log(`[IS] ${this.id}: ${error.message}`);
      });

    if (found) {
      return {
        window: current,
        serviceId: found.serviceId,
        contextId: found.contextId,
        scriptId: found.scriptId
      };
    }

    // TRY LATER
    // // Try to find a router in the sibling frames
    // const siblingFrames = current.frames;
    // for (let i = 0; i < siblingFrames.length; i++) {
    //   const frame = siblingFrames[i];
    //   if (frame === window) continue; // Skip the current frame

    //   const found = await find.call(this, { window: frame, timeout })
    //     .catch((error) => {
    //       this.verbose && console.log(`[IS] ${this.id}: ${error.message}`);
    //     });

    //   if (found) {
    //     return {
    //       window: frame,
    //       serviceId: found.serviceId,
    //       contextId: found.contextId,
    //       scriptId: found.scriptId
    //     };
    //   }
    // }

    if (current === current.parent) break;
    else current = current.parent;
  }

  return null;
}