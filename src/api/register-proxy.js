export default function ({ alias, method, service, configurable = false }) {
  this.api.register({
    method: alias,
    handler: (args, options) => this.api.proxy({ method, service, args, options }),
    configurable
  });
  
  this.verbose && console.log(`[IS] ${this.id}: Registered proxy for ${alias} to ${service}`);
}