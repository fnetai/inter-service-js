export default function ({ where, service } = {}) {

  where = where || {};

  // SERVICE ID CHECK
  if (where.hasOwnProperty('serviceId')) {
    if (Array.isArray(where.serviceId)) {
      if (where.serviceId.length && !where.serviceId.includes(service.serviceId)) return false;
    }
    else if (where.serviceId !== service.serviceId) return false;
  }

  // CONTEXT ID CHECK
  if (where.hasOwnProperty('contextId')) {
    if (Array.isArray(where.contextId)) {
      if (where.contextId.length && !where.contextId.includes(service.contextId)) return false;
    }
    else if (where.contextId !== service.contextId) return false;
  }

  // SCRIPT ID CHECK
  if (where.hasOwnProperty('scriptId')) {
    if (Array.isArray(where.scriptId)) {
      if (where.scriptId.length && !where.scriptId.includes(service.scriptId)) return false;
    }
    else if (where.scriptId !== service.scriptId) return false;
  }

  // ID CHECK
  if (where.hasOwnProperty('id')) {
    if (Array.isArray(where.id)) {
      if (where.id.length && !where.id.includes(service.id)) return false;
    }
    else if (where.id !== service.id) return false;
  }

  // CLIENT CHECK
  if (where.hasOwnProperty('client')) {
    const clients = Object.keys(service.clients);
    if (Array.isArray(where.client)) {
      if (where.client.length && !where.client.some(client => clients.includes(client))) return false;
    }
    else if (!clients.includes(where.client)) return false;
  }

  return true;
}