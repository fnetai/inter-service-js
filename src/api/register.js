export default function ({ method, handler, configurable = false }) {
  if (this.api[method]) throw new Error(`[IS] Cannot register method ${method} as it is already registered.`);

  this.registeredHandlers[method] = handler;

  Object.defineProperty(this.api, method, {
    get: () => handler,
    enumerable: false,
    configurable
  });

  this.verbose && console.log(`[IS] ${this.id}: Registered handler for ${method}`);
}