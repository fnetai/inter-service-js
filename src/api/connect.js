import { nanoid } from 'nanoid';

// @ai: findRouter should be called before connect. findRouter may include target window and service ID.
export default function ({
  window: targetWindow,
  serviceId: targetServiceId
}) {

  if (!targetWindow || !targetServiceId) return Promise.reject(new Error('[IS] Target window and service ID are required to connect to a service'));

  if (targetServiceId === this.serviceId) return Promise.reject(new Error('[IS] Cannot connect to self'));

  return new Promise((resolve, reject) => {
    const transactionId = nanoid();

    const handler = (event) => {
      if (typeof event.data !== 'object') return;

      const { contextId, scriptId, serviceId, messageId, sender } = event.data || {};

      if (!contextId || !scriptId || !serviceId || !messageId || !sender) return;

      if (event.ports.length === 0) return;

      event.ports[0].start();
      this.router = event.ports[0];
      this.router.onmessage = this.handleMessage.bind(this);
      this.router.serviceId = serviceId;
      this.router.onmessageerror = (error) => {
        console.error(`[IS] ${this.id}: Error received from router`, error?.message || error);
      };

      this.verbose && console.log(`[IS] ${this.id}: Communication channel established with ${sender}`);

      this.api.proxy({ method: "echo", args: { ts: Date.now() } })
        .then((response) => {
          event.data.elapsed = (Date.now() - event.data.ts);
          const elapsed = (Date.now() - response.ts);
          this.verbose && console.log(`[IS] ${this.id}: Connection echo test response received from ${sender} in ${elapsed} ms`);
          resolve(event.data.body);
        })
        .catch((error) => {
          reject(error);
        });
    };

    this.transactionHandler[transactionId] = handler;

    const message = this.createMessage({
      transactionId,
      method: 'connect'
    });

    message.body = { where: { serviceId: targetServiceId } };
    
    targetWindow.postMessage(message, '*');

    this.setupTransactionTimeout({ transactionId, reject });
  });
}