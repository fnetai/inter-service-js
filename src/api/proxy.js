import { nanoid } from 'nanoid';
import contextGlobal from '../context/context-global';

export default function ({ method, args, service, options = {} }) {

  return new Promise((resolve, reject) => {
    const transactionId = nanoid();

    const handler = (event) => {
      event.data.elapsed = (Date.now() - event.data.ts);

      if (event.data.error) reject(event.data.error);
      else resolve(event.data?.body);
    };

    this.transactionHandler[transactionId] = handler;

    const ids = (service || '').split('/');
    const clientId = ids.shift();
    const client = this.clients[clientId];

    if (typeof client === 'undefined') {

      if (!this.router) {
        delete this.transactionHandler[transactionId];
        return reject(new Error(`No router registered for ${this.id}`));
      }

      // transaction callback
      if (options?.callback) {
        this.api.register({
          method: transactionId,
          handler: options.callback,
          configurable: true
        });
      }

      const message = this.createMessage({
        transactionId,
        method: method,
        hasCallback: options?.callback ? true : false
      });

      if (args) message.body = args;
      
      if (service) message.service = service;

      this.router.postMessage(message);

      this.setupTransactionTimeout({ transactionId, reject, timeout: options?.timeout });
    }
    else {

      const message = this.createMessage({
        transactionId,
        method: method,
        hasCallback: options?.callback ? true : false
      });

      // transaction callback
      if (options?.callback) {
        this.api.register({
          method: transactionId,
          handler: options.callback,
          configurable: true
        });
      }

      if (args) message.body = args;

      if (ids.length > 0) message.service = ids.join('/');
      
      client.postMessage(message);

      this.setupTransactionTimeout({ transactionId, reject, timeout: options?.timeout });
    }
  });
}