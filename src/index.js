import apiConnect from './api/connect';
import apiPing from './api/ping';
import apiEcho from './api/echo';
import apiRegister from './api/register';
import apiRegisterProxy from './api/register-proxy';
import apiProxy from './api/proxy';
import apiDispose from './api/dispose';
import apiFindRouter from './api/find-router';
import apiUnregister from './api/unregister';
import apiNewId from './api/new-id';

import { nanoid } from 'nanoid';

import contextHandleMessage from './context/handle-message';
import contextSetupTransacttionTimeout from './context/setup-transaction-timeout';
import setupWindowContext from './context/setup-window-context';
import createMessage from './context/create-message.js';

import contextGlobal from './context/context-global';

import EventEmitter from 'eventemitter3';

export default async function ({ id, verbose, router }) {

  const apiContext = {
    api: {}
  };

  async function init() {

    // Validate the name
    if (!/^([a-z\-_]+)$/.test(id)) {
      throw new Error("[IS] Invalid ID format. ID should be lowercase and can include '-' and '_'.");
    }

    this.router_enabled = router;
    this.serviceId = nanoid();
    this.id = id;
    this.verbose = verbose;
    this.ee = new EventEmitter();
    this.router = null;
    this.clients = {};

    this.transactionHandler = {};

    this.registeredHandlers = {};

    this.setupTransactionTimeout = contextSetupTransacttionTimeout.bind(this);
    this.handleMessage = contextHandleMessage.bind(this);
    this.createMessage = createMessage.bind(this);

    await setupWindowContext.apply(this);

    window.addEventListener('message', this.handleMessage);

    contextGlobal.services[this.id] = this;
  }

  // Expose a method with provided name on the api. It can't be proxied.
  function exposeApiMethod(name, handler) {
    Object.defineProperty(apiContext.api, name, {
      get: () => handler,
      enumerable: false,
      configurable: false
    });
  }

  // Expose a method with provided name on the api. It can be proxied.
  function exposeApiProxyMethod(name, handler) {
    const handlerBounded = handler.bind(apiContext);
    apiContext.api.register({ method: name, handler: handlerBounded });
  }

  await init.apply(apiContext);

  // The following methods are exposed as part of the apiContext.api object.
  // Note: The method names are simplified for external use. For example, 
  // 'apiConnect' is exposed as 'connect', 'apiRegister' as 'register', and so on.

  exposeApiMethod('connect', apiConnect.bind(apiContext));        // Exposed as 'connect'
  exposeApiMethod('findRouter', apiFindRouter.bind(apiContext));        // Exposed as 'findRouter'
  exposeApiMethod('register', apiRegister.bind(apiContext));      // Exposed as 'register'
  exposeApiMethod('unregister', apiUnregister.bind(apiContext));      // Exposed as 'unregister'
  exposeApiMethod('registerProxy', apiRegisterProxy.bind(apiContext)); // Exposed as 'registerProxy'
  exposeApiMethod('proxy', apiProxy.bind(apiContext));            // Exposed as 'proxy'
  exposeApiMethod('dispose', apiDispose.bind(apiContext));        // Exposed as 'dispose'
  exposeApiMethod('newId', apiNewId.bind(apiContext));        // Exposed as 'dispose'

  // Expose EventEmitter methods
  exposeApiMethod('on', apiContext.ee.on.bind(apiContext.ee));        // Exposed as 'on'
  exposeApiMethod('once', apiContext.ee.once.bind(apiContext.ee));    // Exposed as 'once'
  exposeApiMethod('off', apiContext.ee.off.bind(apiContext.ee));      // Exposed as 'off'
  exposeApiMethod('addListener', apiContext.ee.addListener.bind(apiContext.ee));      // Exposed as 'addListener'
  exposeApiMethod('removeListener', apiContext.ee.removeListener.bind(apiContext.ee));      // Exposed as 'removeListener'
  exposeApiMethod('removeAllListeners', apiContext.ee.removeAllListeners.bind(apiContext.ee));      // Exposed as 'removeAllListeners'

  exposeApiProxyMethod('ping', apiPing);         // Exposed as 'ping'
  exposeApiProxyMethod('echo', apiEcho);         // Exposed as 'echo'

  return apiContext.api;
}